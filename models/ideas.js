const mongoose = require("mongoose");
const Schema = mongoose.Schema; 

//Model with mongoose Schema, to be stored in db.
const IdeaSchema = new Schema({
    title:      {type: String, required: true,  trim: true, unique: true}, 
    body:       {type: String, required: true,  trim: true}, 
    wows:       {type: String, required: false, default: 0}, 
    area:       {type: String, required: false, default: "Not specified"},
    keywords:   {type: Array,  required: false, default: ["Not specified"]}, 
    author:     {type: mongoose.Schema.Types.ObjectId, ref: "users"}
});

const TaskModel = mongoose.model("ideas", IdeaSchema);

module.exports = TaskModel; 
