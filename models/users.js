const mongoose = require("mongoose");  
const Schema = mongoose.Schema;

const validator = require("validator");  

const bcrypt =  require("bcryptjs"); 

const jwt = require("jsonwebtoken");

const IdeaModel = require("../models/ideas");

const UserSchema = new Schema({
    name:     {type:String, required: true, trim:true}, 
    lastname: {type:String, required: true, trim:true},
    username: {type:String, unique:true, required: true, trim:true},
    email:    {type:String, unique:true, required: true, trim:true, lowercase: true,  
        validate(value){ 
          if(!validator.isEmail(value)){
              throw new Error("Email is not valid. Check and try again please")
          }
        }
    },
    age:             {type: Number, required: false, trim:true},
    city:            {type: String, required: false, trim:true},
    country:         {type: String, required: false, trim:true},
    economicSector:  {type: String, required: false, trim:true},
    password:        {type: String, required: true , trim:true, 
            validate(value){ 
                if(value == "password" || value == "PASSWORD" || value == "123456" || value == "123456789"){ 
                    throw new Error("Try a more complex password please")
                }
            }
    }, 
    tokens:[ 
        { 
            token: {
                type: String, 
                required: true
            }
        }
    ]
}); 

//Checking if credentials match
UserSchema.statics.findByCredentials = async function(email, password) {
    const user = await UserModel.findOne({email}); 
    if(!user){ throw new Error() }

    const validPassword = await bcrypt.compare(password, user.password ); 
    if(!validPassword){ throw new Error(); }

    return user; 
}

//Generating token
UserSchema.methods.generateToken = async function(){
    const user = this;  
    const token =  jwt.sign({_id: user._id.toString()}, "patienecemotherofallsciences")
    user.tokens = user.tokens.concat({ token });
    await user.save();
    return token;
}

//Methods
UserSchema.pre("save", async function (next){ 
    const user = this; 
    if(user.isModified("password")){
        user.password = await bcrypt.hash(user.password, 8);     
    }
    next(); 
})

UserSchema.pre("remove", async function(next){
    await IdeaModel.deleteMany({author: this._id});
    next();
})

//virtual variable
UserSchema.virtual('ideas', {
    ref: 'ideas',
    localField: "_id",
    foreignField: "author"
})

//Controlling outcoming JSON 
UserSchema.methods.toJSON = function(){
    const profile = this.toObject();
    
    delete profile.age; 
    delete profile.city;
    delete profile.country;
    delete profile.password; 
    delete profile.tokens;
    delete profile.__v;
    delete profile._id; 
    
    return profile;
} 


const UserModel = mongoose.model("users", UserSchema); 
module.exports = UserModel; 