const express = require("express");
const app = express();
const PORT = process.env.PORT || 3001;

const morgan = require("morgan");
app.use(morgan("dev"));

require("./database/mongoDB");

const ideasHandler = require("./routes/ideasHanlder");
const usersHandler = require("./routes/usersHandler");  

//Importtant for parsing
app.use(express.json());
app.use("/" , ideasHandler);
app.use("/" , usersHandler); 

app.listen(PORT, ()=> {
    console.log(`Listening on port ${PORT}`);
});
