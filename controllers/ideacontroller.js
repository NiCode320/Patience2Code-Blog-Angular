const IdeaModel = require("../models/ideas");
const ideasController = {};


ideasController.createIdea = async (req, res) => {
    const newIdea = new IdeaModel({...req.body, author: req.user});
    try{
        const idea = await newIdea.save();
        res.status(201).send({status: "Idea created", idea});
    }
    catch(err){
        return res.status(500).send(err);
    }
};

ideasController.readIdeas = async (req, res) => {
    try{
        await req.user.populate("ideas").execPopulate();

        const ideas = req.user.ideas;

        if(ideas.length === 0){ return res.status(200).send({status: "No ideas created"}) };
        res.status(200).send({status: "These are the ideas you have created:", ideas});
    }
    catch(err){
        res.status(500).send(err);
    }
};

ideasController.readIdea = async (req, res) => {
    try{
        const idea = await IdeaModel.findOne({_id: req.params.id, author:req.user._id});
        if(!idea){ return res.status(200).send({status: "Idea not founded"})}
        await res.status(200).send(idea);
    }
    catch(err){
        res.status(500).send(err); 
    }
};

ideasController.updateIdea = async (req, res) => {
    const updates = Object.keys(req.body); 
    const allowableUpdates = ["title", "body","area", "keywords"]; 

    const validOperation = updates.every(update => allowableUpdates.includes(update)); 
    if(!validOperation){  return res.status(400).send({error:"Not Valid operation"}) };

    try{
        const idea = await IdeaModel.findOne({_id:req.params.id, author: req.user._id}); 
        if(!idea){ return res.status(400).send(err)}
        
        updates.forEach( update => idea[update] = req.body[update] );
        await idea.save();
        
        res.status(200).send(idea); 
    }
    catch(err){
        res.status(500).send(error); 
    }    
};

ideasController.deleteIdea = async (req, res) => {
    try{
        await IdeaModel.findOneAndDelete({_id:req.params.id, author: req.user._id});
        res.status(200).send({status: "Idea deleted" });
    }
    catch(err){
        res.status(500).send(err);
    }
};

module.exports = ideasController;


