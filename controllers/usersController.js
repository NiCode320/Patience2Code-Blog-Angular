const UsersModel = require("../models/users");
const usersControllers = {}; 


usersControllers.createUser = async(req,res) => {
    try{
        const user = await new UsersModel(req.body); 
        await user.save(); 

        res.status(200).send({status: "User created",user}); 
    }
    catch(err){
        res.status(500).send(err); 
    }
     
};

usersControllers.readProfile = async(req,res)=> {
    try{
        const user = req.user;
        res.status(200).send(user); 
    }
    catch(err){ 
        res.status(500).send(err); 
    }
};


usersControllers.updateUser = async(req,res) => {
    const updates = Object.keys(req.body);
    const allowableUpdates = ["name","lastname","email","age","city",
                                "country","economicSector","password"]; 

    const validOperation = updates.every(update => allowableUpdates.includes(update) ); 
    if(!validOperation){ return res.status(400).send({error: "Invalid update"});}
    
    const user = req.user;
    
    try{ 
        updates.forEach(update => user[update] = req.body[update] ); 
        await user.save(); 
        res.status(200).send({status:"User updated" ,user}); 
    }
    catch(err){ 
        res.status(500).send(err);
    }
}; 

usersControllers.deleteUser = async(req,res) => {
    try{
        await req.user.remove() 
        res.status(200).send({status: "User deleted"})
    }
    catch(err){
        res.status(500).send({status: "Deletion failed."})
    }
}; 

//Login methods 
usersControllers.loginAccount = async(req,res) => {
    try{
        const data = req.body; 
        const user =  await UsersModel.findByCredentials(data.email,data.password); 
        const token = await user.generateToken(); 
        res.status(200).send({status: "Welcome "+ user.name , user, token});
    }
    catch(err){ 
        res.status (400).send({error:"Email or password doesn't match"});        
    }
}

//logout
usersControllers.logout = async(req,res) => {
    try{
        req.user.tokens = req.user.tokens.filter(token => {
            return token.token != req.token; 
        })
        await req.user.save();
        res.status(200).send({status: "Succesfully logged out"})
    }
    catch(err){
        res.status(500).send(err);
    }
   
}

usersControllers.logoutAll = async(req,res) => {
    try{
        req.user.tokens = [];     
        await req.user.save(); 
        res.status(200).send({status: "Logged out from all sessions"})
    }
    catch(err){
        res.status(500).send(err);
    }
}

module.exports = usersControllers; 