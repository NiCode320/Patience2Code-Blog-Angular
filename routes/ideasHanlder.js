const express = require("express");
const router = express.Router();

const IdeaHandler = require("../controllers/ideacontroller");

const auth = require("../middlewares/authentication")

router.post  ("/post-idea",       auth, IdeaHandler.createIdea);
router.get   ("/ideas",           auth, IdeaHandler.readIdeas);
router.get   ("/idea/:id",        auth, IdeaHandler.readIdea);
router.patch ("/edit-idea/:id",   auth, IdeaHandler.updateIdea);
router.delete("/delete-idea/:id", auth, IdeaHandler.deleteIdea );



module.exports = router;