const express = require("express"); 
const router = express.Router(); 

const usersHandler = require("../controllers/usersController"); 

const authentication = require("../middlewares/authentication"); 

router.post  ("/register/user" ,                 usersHandler.createUser); 
router.get   ("/user/me"       , authentication, usersHandler.readProfile); 
router.patch ("/update/user/me", authentication, usersHandler.updateUser); 
router.delete("/delete/user/me", authentication, usersHandler.deleteUser); 

// Login Routers 
router.post  ("/login",                           usersHandler.loginAccount)
router.post  ("/logout",          authentication, usersHandler.logout)
router.post  ("/logout/all",      authentication, usersHandler.logoutAll)

module.exports = router; 