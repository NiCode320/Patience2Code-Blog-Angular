const mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);

const username = process.env.MONGO_DB_USERNAME,
      password = process.env.MONGO_DB_PASSWORD;
const URI = `mongodb+srv://${username}:${password}@firstcluster-eldi8.mongodb.net/patience2code?retryWrites=true&w=majority`;

mongoose.connect(URI,
    { useUnifiedTopology: true, useNewUrlParser: true })
    .then(db => console.log("MongoDB is connected"))
    .catch(err => console.log(">> ERROR: ",err));

module.exports = mongoose;



