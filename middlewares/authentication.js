const UserModel = require("../models/users");

const jwt = require("jsonwebtoken");

const authentication = async(req,res,next) => {
    try{
        const token = req.header("Authorization").replace("Bearer ", "");

        const validToken = jwt.verify(token, "patienecemotherofallsciences");

        const user = await UserModel.findOne({_id: validToken._id,"tokens.token": token});
    
        if(!user){ throw new Error() }
        
        req.user = user; 
        req.token = token;
        next();
    }
    catch(err){
        res.status(500).send({error: "Please authenticate"})
    }
}
module.exports = authentication; 